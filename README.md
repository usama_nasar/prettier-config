# Prettier config for Ptojects

A shareable [Prettier](https://prettier.io/) config for projects.

## Installation

Run `npm i -D icode-prettier-config` to install this package.

Also run following command to install its required dependencies

`npm i -D prettier`

### Make compatible with eslint

Run `npm i -D eslint-config-prettier` to install this package.

### Make compatible with stylelint

Run `npm i -D stylelint-config-prettier` to install this package.

### Configure Prettier to run on commit

Run `npm i -D husky lint-staged` to install this package.

After running this command, add this to package.json to enable eslint on commit

    "scripts": {
        "prettier": "prettier --check \"**/*.{css,html,js,json,scss,ts}\""
    },
    "husky": {
    	"hooks": {
    		"pre-commit": "lint-staged"
    	}
    },
    "lint-staged": {
        "*.{html,json}": [
    		"prettier --write",
    		"git add"
    	],
    	"*.{js,ts}": [
    		"prettier --write",
    		"git add"
    	],
        "*.{css,scss}": [
            "prettier --write",
            "git add"
        ]
    },
    "prettier": "icode-prettier-config"
